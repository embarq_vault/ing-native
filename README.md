## Concepts

- MVC
  - View: HTML & CSS
  - Controller: JS function or class
  - Model: object or class
- Reactive programming
  - pure functions 
  - high-ordered functions and callbacks
  - event-driven programming
    - event emitter pattern
  - data flow
  - immutable data operations

## ToDo app

##### model

```ts
type Todo = {
  title: string;
  done: boolean;
  createdAt: string;
  updatedAt: string;
  labelColor: string;
  position: number;
}
```

### Tasks

1. List todos
2. Create todo
3. Update todo
  - toggle state
  - edit title
  - edit labelColor
  - edit position
