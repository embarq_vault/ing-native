class EventEmitter {
  constructor() {
    this.handlers = [];
  }

  emit(data) {
    for (let handler of this.handlers) {
      handler(data);
    }
  }

  subscribe(handler) {
    this.handlers.push(handler);
  }
}

/** @abstract */
class View {
  render() {
    throw new Error('Not implemented');
  }
}

/** @abstract */
class Controller {
  getView(renderData) {
    throw new Error('Not implemented');
  }
}

class TodoLabelPicker {}

class TodoCheckboxComponent extends Controller {

  constructor() {
    super();
    this.change = new EventEmitter();
  }

  /**
   * @description initilize related view instance.
   * This method initialized events handling for the view
   * @returns {void}
   */
  initView(options) {
    const viewEvents = {
      change: (event) => {
        this.change.emit(event.target.checked);
      }
    };
    this.view = new TodoCheckboxView(options, viewEvents);
  }

  onChange(handler) {
    this.change.subscribe(handler);
  }

  /**
   * @description renders component's view and returns as complete html element
   * @param {{[key: string]: any}} renderData data to be inserted into a render cycle
   * @returns {HTMLElement}
   **/
  getView(renderData) {
    return this.view.render(renderData);
  }
}

class TodoCheckboxView extends View {
  /**
   * @param {{ [htmlElementProperty: string]: any }} options
   * @param {{ [eventName: string]: (event: Event) => void }} events
   */
  constructor(options, events) {
    super();

    this.id = options.id || null;
    this.className = options.className || null;
    this.events = events || {};
  }

  /**
   * @param { {[key: string]: any} } renderData
   * @returns {HTMLElement}
   */
  render(renderData) {
    const elem = document.createElement('input');
    elem.type = 'checkbox';
    elem.id = this.id;
    elem.className = this.className;
    elem.checked = false;

    for (prop in (renderData)) {
      if (renderData.hasOwnProperty(prop)) {
        elem[prop] = renderData[prop];
      }
    }

    for (let eventName in this.events) {
      elem.addEventListener(eventName, this.events[eventName]);
    }

    return elem;
  }
}

/**
 * @param {string} id
 * @param {string} title
 * @param {boolean} done
 * @param {string} createdAt
 * @param {string} updatedAt
 * @param {string} labelColor
 * @param {number} position
 */
class TodoItemComponent extends Controller {
  constructor(data) {
    super();
    this.data = data;
  }

  /** @returns {void} */
  initView() {
    const checkbox = new TodoCheckboxComponent();
    checkbox.initView({
      id: this.data.id,
      className: 'form-control'
    });

    const todoItemViewParams = { id: this.data.id + '_checkbox' };
    const todoItemViewEvents = {};
    const todoItemViewDependencies = { checkbox };

    this.view = new TodoItemView(
      todoItemViewParams,
      todoItemViewEvents,
      todoItemViewDependencies
    );
  }

  /**
   * @description renders component's view and returns as complete html element
   * @param {{[key: string]: any}} renderData data to be inserted into a render cycle
   * @returns {HTMLElement}
   **/
  getView() {
    const { title, done } = this.data;
    return this.view.render({ title, done });
  }
}

class TodoItemView extends View {
  constructor(options, events, viewDependencies) {
    super();

    this.id = options.id || null;
    this.className = options.className || null;
    this.viewDependencies = viewDependencies;
  }

  /**
   * @description renders component's view and returns as complete html element
   * @param {{[key: string]: any}} renderData data to be inserted into a render cycle
   * @returns {HTMLElement}
   **/
  render(renderData) {
    const itemTemplate = document.getElementById('templ:todo-item');
    const itemComponent = document.importNode(itemTemplate.content, true);

    itemComponent.id = this.id;
    itemComponent.className = this.className;

    const label = itemComponent.querySelector('[data-bind-child]');
    label.querySelector('[data-bind-title]').innerText = renderData.title;

    const checkboxElement =
      this.viewDependencies.checkbox.getView({
        checked: renderData.done
      });

    label.setAttribute('for', checkboxElement.id);
    label.prepend(checkboxElement);

    return itemComponent;
  }
}

class TodoList {}

class TodoListView extends View {
  render() {
    return null;
  }
}

class TodoApp {
  constructor() {
    const host = document.getElementById('app-root');
    const todoList = new TodoList([]);

    const todoListItem = new TodoItemComponent({
      id: 'asduhasiud91asdj98h98dw1w83gh98sjad',
      title: 'Test title',
      done: false
    });
    todoListItem.initView();

    host.appendChild(todoListItem.getView());
  }
}


window.addEventListener('DOMContentLoaded', () => {
  const app = new TodoApp();
})
